<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_thm_backendlayout', 'Configuration/TypoScript', 'hive_thm_backendlayout');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/***************
 * Setup EXTKEY
 * was null on Typo3 10
 ****************/
$_EXTKEY ='hive_thm_backendlayout';
$extKey = $_EXTKEY;

call_user_func(
    function($_EXTKEY)
    {
        /***************
         * Add page TSConfig
         */
        $pageTsConfig = \TYPO3\CMS\Core\Utility\GeneralUtility::getUrl(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'Configuration/TsConfig/Page/config.txt');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($pageTsConfig);

    }, $_EXTKEY
);